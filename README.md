```
git clone git@gitlab.com:RobinHoutevelts/aws-iot.git
cd aws-iot
yarn

npm install -g browserify
npm run-script browserize examples/browser/lifecycle/index.js

cd examples/browser/lifecycle

python -m SimpleHTTPServer 8001
```

Surf naar https://localhost:8001 en check u console.log

Ge zou normaal connected moeten zijn met de websocket en subscribed op '/wtfock/stag/posts/stats'.

Als ge in AWS-land IoT-Core opendoet en iets manueel published op da topic (kan zijn dat ge rechten moet vragen) dan zou ge da moeten zien binnenkomen.